package paint.Controller;

import paint.View.PaintFrame;
import paint.View.PaintPanel;
import paint.model.*;
import paint.model.Polygon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.ArrayList;
import java.util.TreeSet;


/**
 * Steuert den Programmablauf des Paint-Programmes
 * @author Kacper Urbaniec
 * @version 27.05.2018
 */
public class PaintController implements ActionListener, MouseListener, MouseMotionListener, KeyListener {
    private ArrayList<Point> pt;
    private PaintFrame frame;
    private PaintPanel panel;
    private Speicher speicher;
    private boolean create = true;
    private boolean track = false;
    private String auswahl = "";
    private TreeSet<Integer> keys = new TreeSet<>();

    /**
     * Erzeugt ein neues Controller-Objekt.
     */
    public PaintController() {
        panel = new PaintPanel(this);
        frame = new PaintFrame("Zeichenbrett", panel);
        pt = new ArrayList<>();
        speicher = new Speicher();
    }

    /**
     * Regelt die Ereignissteuerung des Programmes.
     *
     * @param e Das zu verarbeitende Event
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String s = e.getActionCommand();
        switch (s) {
            case "neu":
                // if-Anweisung, damit beim Erstellen von Elementen kein
                // Reset möglich ist
                if(create) {
                    // Speicher neu initialisieren und Panel "reseten"
                    speicher = new Speicher();
                    panel.setElements(speicher.getElements());
                    panel.setBackgroundColor(speicher.getBackground());
                }
                break;
            case "laden":
                // if-Anweisung, damit beim Erstellen von Elementen kein
                // Laden möglich ist
                if(create) {
                    try{
                        // Dialogfeld zum Laden öffnen
                        JFileChooser chooser = new JFileChooser();
                        chooser.showOpenDialog(null);
                        // Pfad speichern
                        // Streams erstellen und Laden des Objektes
                        File file = new File(chooser.getSelectedFile().getAbsolutePath());
                        FileInputStream fin = new FileInputStream(file.getAbsoluteFile());
                        ObjectInputStream in = new ObjectInputStream(fin);
                        speicher = (Speicher) in.readObject();
                        // Zu zeichnende Elemente "reseten"
                        panel.setElements(speicher.getElements());
                        // Hintergrundfarbe setzen
                        Color hg = speicher.getBackground();
                        if(hg != null) {
                            panel.setBackgroundColor(hg);
                        }
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, "Etwas ist schief gelaufen", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
                break;
            case "speichern":
                // if-Anweisung, damit beim Erstellen von Elementen kein
                // Speichern möglich ist
                if(create) {
                    try{
                        // Dialogfeld zum Speichern öffnen
                        JFileChooser chooser = new JFileChooser();
                        chooser.showSaveDialog(null);
                        // Pfad speichern
                        File file = new File(chooser.getSelectedFile().getAbsolutePath());
                        // Streams erstellen und Schreiben des Objektes ausführen
                        FileOutputStream fout = new FileOutputStream(file.getAbsoluteFile());
                        ObjectOutputStream out = new ObjectOutputStream(fout);
                        out.writeObject(speicher);
                        out.flush();
                        out.close();
                    } catch (IOException ex) {
                        JOptionPane.showMessageDialog(null, "Etwas ist schief gelaufen", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
                break;
            case "loe":
                // if-Anweisung, damit beim Erstellen von Elementen kein
                // "Element löschen" ausgeführt werden kann
                if(create) {
                    speicher.undo();
                    panel.setElements(speicher.getElements());
                }
                break;
            case "wied":
                // if-Anweisung, damit beim Erstellen von Elementen kein
                // "Element wiederherstellen" ausgeführt werden kann
                if(create) {
                    speicher.redo();
                    panel.setElements(speicher.getElements());
                }
                break;
            case "dup":
                // if-Anweisung, damit beim Erstellen von Elementen kein
                // Duplizieren möglich ist
                if(create) {
                    // Letztes Element beziehen und in Panel und Speicher hinzufügen
                    ElementBasic dup = (ElementBasic) panel.getLastElement().clone();
                    panel.addElement(dup);
                    speicher.addElement(dup);
                }
                break;
            case "homepos":
                panel.setHomePosition();
                break;
            case "efarbe":
                break;
            case "stift":
                Color sc = panel.showSChooser();
                panel.setStiftBackground(sc);
                break;
            case "hinterg":
                Color hg = panel.showHChooser();
                panel.setHintergBackground(hg);
                panel.setBackgroundColor(hg);
                speicher.setBackground(hg);
                break;
            case "hilfe":
                JOptionPane.showMessageDialog(null,
                        "Anleitung\n" +
                                "Die Modi Freihand, Linie, Rechteck und Ellipsen werden mit gehaltener Maustaste und Mausbewegung gezeichnet. Das Loslassen der Maustaste bestätigt das Element.\n" +
                                "Beim Modus Polygon werden die Punktes durch Mausklicks erzeugt. Das Element wird durch die Enter-Taste bestätigt.\n"+
                                "Elemente können mit Hilfe der Pfeiltasten verschoben werden. Mit gedrückter Shift-Taste ist die Verschiebung schneller.",
                        "Hilfe", JOptionPane.PLAIN_MESSAGE);
                break;
            case "about":
                JOptionPane.showMessageDialog(null,
                        "Version 1.0\nCreated by\nKacper Urbaniec & Martin Wustinger",
                        "About", JOptionPane.PLAIN_MESSAGE);

        }
    }


    /**
     * Triggert wenn ein Maus-Button gedrückt wurde. Wird für das Festlegen der
     * Punkt für das Polygon verwendet.
     * @param e Das Listener-Event
     */
    @Override
    public void mousePressed(MouseEvent e) {
        // Zeichenmodus erhalten
        auswahl = panel.getAuswahl();
        // Schauen ob Polygon ausgewählt ist
        if(auswahl.equals("polygone") || auswahl.equals("polygoneA")) {
            // Punkt speichern
            pt.add(new Point(e.getX(), e.getY()));
            // Falls schon ein Polygon gezeichnet wurde, dieses entfernen, weil
            // es neu erweitert wird.
            if (!create) {
                panel.delElement();
            }
            // create-flag auf false setzen, da der erste Durchlauf fast schon
            // beendet wurde
            create = false;
            // Stiftfarbe erhalten
            Color c = panel.getStiftC();
            // Je nach Polygon-Art, ein Polygon zeichnen
            if(auswahl.equals("polygone")) {
                panel.addElement(new Polygon(c, pt));
            }
            else {
                panel.addElement(new Polygon(c, pt, true));
            }
        }
    }

    /**
     * Triggert wenn eine Taste losgelassen wird. Wird verwendet um ein fertiges
     * Polygon mit der Enter-Taste zu bestätigen.
     * @param e Das Listener-Event
     */
    @Override
    public void keyReleased(KeyEvent e) {
        // KeyCode erhalten und schauen ob die ENTER-Taste gedrückt wurde.
        // Außerdem schauen ob der Polygon-Modus ausgewählt wurde.
        int key = e.getKeyCode();
        auswahl = panel.getAuswahl();
        if (key == KeyEvent.VK_ENTER && (auswahl.equals("polygone") || auswahl.equals("polygoneA"))) {
            // create-flag auf true setzen, da ab jetzt neue Elemente erstellt
            // werden sollen
            create = true;
            // Dem Speicher fertiges Element übergeben
            speicher.addElement(panel.getLastElement());
            // Punkte-Array neu initialisieren.
            pt = new ArrayList<>();
        }
        // Wird für das Verschieben von ELmenten verwendet.
        // Löscht gedrückte Tasten aus dem keys-Array
        keys.remove(key);
    }

    /**
     * Triggert wenn eine Maus-Taste gedrückt ist und eine Mausbewegung zugleich
     * stattfindet. Wird zum Erstellen aller neuer Elemente, außer Polygone
     * verwendet.
     *
     * @param e Das Listener-Event
     */
    @Override
    public void mouseDragged(MouseEvent e) {
        // Zeichenmodus erhalten
        auswahl = panel.getAuswahl();
        // If damit Polygone nicht beeinflusst werden (arbeiten stattdessen mit
        // mousePressed und keyReleased
        if(!auswahl.equals("polygone") && !auswahl.equals("polygoneA") && !auswahl.equals("")) {
            // Schauen ob Methode aufgerufen wurde
            track = true;
            // Punkt speichern
            Point p= new Point(e.getX(), e.getY());
            pt.add(p);
            // Alle Elemente in dieser Methode arbeiten mit Zwei Punkten, außer
            // Freihand, deshalb müssen bei diesen immer konstant zwei Punkte
            // vorhanden sein
            if (!auswahl.equals("freih") && pt.size() > 2) {
                pt.remove(pt.size()-2);
            }
            // Vorherige, unfertige Elemente löschen, außer beim ersten
            // Zeichenvorgang eines neuen Elementes
            if (!create) {
                panel.delElement();
            }
            // Zweiten Punkt hinzufügen beim ersten Zeichenvorgang übergeben,
            // außer beim Freihandzeichnen und Ursprung setzen.
            else {
                ElementBasic.setUrsprung(p);
                if (!auswahl.equals("freih")) {
                    pt.add(new Point(e.getX(), e.getY()));
                }
            }
            // create-flag auf false setzen, da der erste Durchgang fast beendet ist
            create = false;
            // Stiftfarbe auswählen und Objekt zeichnen
            Color c = panel.getStiftC();
            switch (auswahl) {
                case "freih":
                    panel.addElement(new FreihandZeichnung(c, pt));
                    break;
                case "linie":
                    panel.addElement(new Linie(c, pt));
                    break;
                case "rechteck":
                    panel.addElement(new Rechteck(c, pt));
                    break;
                case "rechteck2":
                    panel.addElement(new RechteckAG(c, pt));
                    break;
                case "ellipse":
                    panel.addElement(new Ellipse(c, pt));
                    break;
                case "rechteckA1":
                    panel.addElement(new Rechteck(c, pt, true));
                    break;
                case "rechteckA2":
                    panel.addElement(new RechteckAG(c, pt, true));
                    break;
                case "ellipseA":
                    panel.addElement(new Ellipse(c, pt, true));
                    break;
            }
        }
    }

    /**
     * Triggert wenn eine Maus-Taste losgelassen wird. Dient zum Abschießen aller
     * Elemente, außer von Polygonen.
     * @param e the event to be processed
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        if(track) {
            // create-flag auf true setzen, da ab jetzt neue Elemente erstellt
            // werden sollen
            create = true;
            // Dem Speicher fertiges Element übergeben
            speicher.addElement(panel.getLastElement());
            // Punkte-Array neu initialisieren.
            pt = new ArrayList<>();
            // Methode mouseDragged muss wieder aufgerufen werden
            track = false;
        }
    }

    /**
     * Triggert solange eine Taste gedrückt und gehalten wird. Wird verwendet um
     * das letze Element mit den Pfeiltasten zu verschieben. Wenn man dabei die
     * Shift-Taste mithält, werden die Elemente schneller verschoben
     * @param e Das Listener Event
     */
    @Override
    public void keyPressed(KeyEvent e) {
        // KeyCode erhalten und in das Set hinzufügen
        int pressed = e.getKeyCode();
        keys.add(pressed);
        // Speed auf 1 lassen, außer die Shift-Taste wird mitgehalten
        int speed = 1;
        if (keys.contains(KeyEvent.VK_SHIFT)) {
            speed = 10;
        }
        // Je nach Pfeiltaste in die richtige Richtung verschieben
        for(int key : keys) {
            if (key == KeyEvent.VK_UP) {
                panel.verschieben(0, -speed);
            } else if (key == KeyEvent.VK_DOWN) {
                panel.verschieben(0, speed);
            } else if (key == KeyEvent.VK_LEFT) {
                panel.verschieben(-speed, 0);
            } else if (key == KeyEvent.VK_RIGHT) {
                panel.verschieben(speed, 0);
            }
        }

    }

    /**
     * Startet das Paint-Programm
     * @param args Argumente (nicht benutzt)
     */
    public static void main(String[] args) {
        new PaintController();
    }



    // Nicht verwendete Listener-Events

    /**
     * Invoked when the mouse button has been clicked (pressed
     * and released) on a component.
     *
     * @param e the event to be processed
     */
    @Override
    public void mouseClicked(MouseEvent e) {

    }

    /**
     * Invoked when the mouse enters a component.
     *
     * @param e the event to be processed
     */
    @Override
    public void mouseEntered(MouseEvent e) {

    }

    /**
     * Invoked when the mouse exits a component.
     *
     * @param e the event to be processed
     */
    @Override
    public void mouseExited(MouseEvent e) {

    }

    /**
     * Invoked when the mouse cursor has been moved onto a component
     * but no buttons have been pushed.
     *
     * @param e the event to be processed
     */
    @Override
    public void mouseMoved(MouseEvent e) {
    }

    /**
     * Invoked when a key has been typed.
     * See the class description for {@link KeyEvent} for a definition of
     * a key typed event.
     *
     * @param e the event to be processed
     */
    @Override
    public void keyTyped(KeyEvent e) {

    }


}

