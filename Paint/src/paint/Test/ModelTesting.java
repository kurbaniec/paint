package paint.Test;
import paint.model.*;
import paint.model.Polygon;
import org.junit.*;
import java.awt.*;
import java.util.*;
import static org.junit.Assert.*;

/**
 * Test der Model Klassen
 * @author Martin Wustinger
 * @version 2018-06-03
 */
public class ModelTesting {
	ArrayList<Point> p1;
	ArrayList<ElementBasic> ele;
	Speicher s1;
	Speicher s2;
	@Before
	public void setUp() {
		//F�r Ellipse, linie und Rechtecke
		p1 = new ArrayList<>(); 
		p1.add(new Point(0, 0));
		p1.add(new Point(100, 100));
		//Beinhaltet alle Elemente 
		ele = new ArrayList<>(); 
		ele.add(0, new Ellipse());
		ele.add(1, new Ellipse(Color.BLUE));
		ele.add(new Ellipse(Color.BLUE, p1));
		ele.add(new Ellipse(Color.BLUE, p1, false));
		ele.add(new Rechteck());
		ele.add(new Rechteck(Color.BLUE));
		ele.add(new Rechteck(Color.BLUE, p1));
		ele.add(new Rechteck(Color.BLUE, p1, false));
		ele.add(new RechteckAG());
		ele.add(new RechteckAG(Color.BLUE));
		ele.add(new RechteckAG(Color.BLUE, p1));
		ele.add(new RechteckAG(Color.BLUE, p1, false));
		ele.add(new Linie());
		ele.add(new Linie(Color.BLUE));
		ele.add(new Linie(Color.BLUE, p1));
		ele.add(new FreihandZeichnung());
		ele.add(new FreihandZeichnung(Color.BLUE));
		ele.add(new FreihandZeichnung(Color.BLUE, p1));
		ele.add(new Polygon());
		ele.add(new Polygon(Color.BLUE));
		ele.add(new Polygon(Color.BLUE, p1));
		ele.add(new Polygon(Color.BLUE, p1, false));
		//Speicher
		s1 = new Speicher();
		s2 = new Speicher();
		s2.addElement(ele.get(0));
	}	
	// **************
	// Konstruktoren
	// **************
	@Test
	public final void testKonstruktorEllipse1() {
		assertEquals(ele.get(0), new Ellipse());
	}
	@Test
	public final void testKonstruktorEllipse2() {
		assertEquals(ele.get(1), new Ellipse(Color.BLUE));
	}
	@Test
	public final void testKonstruktorEllipse3() {
		assertEquals(ele.get(2), new Ellipse(Color.BLUE, p1));
	}
	@Test
	public final void testKonstruktorEllipse4() {
		assertEquals(ele.get(3), new Ellipse(Color.BLUE, p1, false));
	}
	@Test
	public final void testKonstruktorRechteck1() {
		assertEquals(ele.get(4), new Rechteck());
	}
	@Test
	public final void testKonstruktorRechteck2() {
		assertEquals(ele.get(5), new Rechteck(Color.BLUE));
	}
	@Test
	public final void testKonstruktorRechteck3() {
		assertEquals(ele.get(6), new Rechteck(Color.BLUE, p1));
	}
	@Test
	public final void testKonstruktorRechteck4() {
		assertEquals(ele.get(7), new Rechteck(Color.BLUE, p1, false));
	}
	@Test
	public final void testKonstruktorRechteckAG1() {
		assertEquals(ele.get(8), new RechteckAG());
	}
	@Test
	public final void testKonstruktorRechteckAG2() {
		assertEquals(ele.get(9), new RechteckAG(Color.BLUE));
	}
	@Test
	public final void testKonstruktorRechteckAG3() {
		assertEquals(ele.get(10), new RechteckAG(Color.BLUE, p1));
	}
	@Test
	public final void testKonstruktorRechteckAG4() {
		assertEquals(ele.get(11), new RechteckAG(Color.BLUE, p1, false));
	}
	@Test
	public final void testKonstruktorLinie1() {
		assertEquals(ele.get(12), new Linie());
	}
	@Test
	public final void testKonstruktorLinie2() {
		assertEquals(ele.get(13), new Linie(Color.BLUE));
	}
	@Test
	public final void testKonstruktorLinie3() {
		assertEquals(ele.get(14), new Linie(Color.BLUE, p1));
	}
	@Test
	public final void testKonstruktorFreihandZeichnung1() {
		assertEquals(ele.get(15), new FreihandZeichnung());
	}
	@Test
	public final void testKonstruktorFreihandZeichnung2() {
		assertEquals(ele.get(16), new FreihandZeichnung(Color.BLUE));
	}
	@Test
	public final void testKonstruktorFreihandZeichnung3() {
		assertEquals(ele.get(17), new FreihandZeichnung(Color.BLUE, p1));
	}
	@Test
	public final void testKonstruktorPolygon1() {
		assertEquals(ele.get(18), new Polygon());
	}
	@Test
	public final void testKonstruktorPolygon2() {
		assertEquals(ele.get(19), new Polygon(Color.BLUE));
	}
	@Test
	public final void testKonstruktorPolygon3() {
		assertEquals(ele.get(20), new Polygon(Color.BLUE, p1));
	}
	@Test
	public final void testKonstruktorPolygon4() {
		assertEquals(ele.get(21), new Polygon(Color.BLUE, p1, false));
	}
	
	// **************
	// Clone
	// **************
	@Test
	public final void testCloneEllipse() {
		assertEquals(ele.get(3), ele.get(3).clone());
	}
	@Test
	public final void testCloneRechteck() {
		assertEquals(ele.get(7), ele.get(7).clone());
	}
	@Test
	public final void testCloneRechteckAG() {
		assertEquals(ele.get(11), ele.get(11).clone());
	}
	@Test
	public final void testCloneLinie() {
		assertEquals(ele.get(14), ele.get(14).clone());
	}
	@Test
	public final void testCloneFreihandZeichnung() {
		assertEquals(ele.get(17), ele.get(17).clone());
	}
	@Test
	public final void testClonePolygon() {
		assertEquals(ele.get(21), ele.get(21).clone());
	}
	
	// **************
	// ElementBasic
	// **************
	@Test
	public final void testAddPoint() {
		ArrayList<Point> ptest = new ArrayList<>();
		ptest.add(new Point(0, 0));
		ElementBasic expected = ele.get(17);
		ElementBasic actual = new FreihandZeichnung(Color.BLUE, ptest);
		actual.addPoint(100, 100);
		assertEquals(expected, actual);
	}
	@Test
	public final void testRemoveLastPoint() {
		ArrayList<Point> ptest = new ArrayList<>();
		ptest.add(new Point(0, 0));
		ptest.add(new Point(100, 100));
		ptest.add(new Point(200, 200));
		ElementBasic expected = ele.get(17);
		ElementBasic actual = new FreihandZeichnung(Color.BLUE, ptest);
		actual.removeLastPoint();
		assertEquals(expected, actual);
	}
	@Test
	public final void testChangePoint() {
		ArrayList<Point> ptest = new ArrayList<>();
		ptest.add(new Point(0, 0));
		ptest.add(new Point(200, 200));
		ElementBasic expected = ele.get(17);
		ElementBasic actual = new FreihandZeichnung(Color.BLUE, ptest);
		actual.changePoint(1, 100, 100);
		assertEquals(expected, actual);
	}
	@Test
	public final void testCheckObAufZF1() {
		assertEquals(true, ElementBasic.checkObAufZF(new Point(100, 100)));
	}
	@Test
	public final void testCheckObAufZF2() {
		assertEquals(false, ElementBasic.checkObAufZF(new Point(-100, -100)));
	}
	@Test
	public final void testVerschieben1() {
		ArrayList<Point> ptest = new ArrayList<>();
		ptest.add(new Point(100, 100));
		ptest.add(new Point(200, 200));
		ElementBasic expected = ele.get(17);
		ElementBasic actual = new FreihandZeichnung(Color.BLUE, ptest);
		actual.verschieben(-100, -100, 1000, 1000);
		assertEquals(expected, actual);
	}
	@Test
	public final void testVerschieben2() {
		ArrayList<Point> ptest = new ArrayList<>();
		ptest.add(new Point(900, 900));
		ptest.add(new Point(1000, 1000));
		ElementBasic expected = new FreihandZeichnung(Color.BLUE, ptest);
		ElementBasic actual = (ElementBasic) ele.get(17).clone();
		actual.verschieben(10000, 10000, 1000, 1000);
		assertEquals(expected, actual);
	}
	@Test
	public final void testVerschieben3() {
		ArrayList<Point> ptest = new ArrayList<>();
		ptest.add(new Point(0, 0));
		ptest.add(new Point(100, 100));
		ElementBasic expected = new FreihandZeichnung(Color.BLUE, ptest);
		ElementBasic actual = (ElementBasic) ele.get(17).clone();
		actual.verschieben(-10000, -10000, 1000, 1000);
		assertEquals(expected, actual);
	}
	@Test
	public final void testRearrange1() {
		ElementBasic.setUrsprung(new Point(100, 100));
		ArrayList<Point> ptest = new ArrayList<>();
		ptest.add(new Point(100, 100));
		ptest.add(new Point(0, 0));
		ElementBasic expected = ele.get(17);
		ElementBasic actual = new FreihandZeichnung(Color.BLUE, ptest);
		actual.rearrange();
		assertEquals(expected, actual);
	}
	@Test
	public final void testRearrange2() {
		ElementBasic.setUrsprung(new Point(100, 100));
		ArrayList<Point> ptest = new ArrayList<>();
		ptest.add(new Point(100, 0));
		ptest.add(new Point(200, 100));
		ArrayList<Point> ptest2 = new ArrayList<>();
		ptest2.add(new Point(100, 100));
		ptest2.add(new Point(200, 0));
		ElementBasic expected = new FreihandZeichnung(Color.BLUE, ptest);
		ElementBasic actual = new FreihandZeichnung(Color.BLUE, ptest2);
		actual.rearrange();
		assertEquals(expected, actual);
	}
	@Test
	public final void testpointClone() {
		assertEquals(p1, ele.get(3).pointClone());
	}
	@Test
	public final void testHashCode() {
		assertEquals(-912078038, ele.get(3).hashCode());
	}
	@Test
	public final void testEquals1() {
		assertEquals(true, ele.get(3).equals(ele.get(3).clone()));
	}
	@Test
	public final void testEquals2() {
		assertEquals(false, ele.get(3).equals(ele.get(7)));
	}
	//Setter Getter
	@Test
	public final void testGetColor() {
		assertEquals(Color.BLUE, ele.get(3).getColor());
	}
	@Test
	public final void testSetColor() {
		ElementBasic e = (ElementBasic) ele.get(3).clone();
		e.setColor(Color.BLACK);
		assertEquals(Color.BLACK, e.getColor());
	}
	@Test
	public final void testGetFill() {
		assertEquals(false, ele.get(3).getFill());
	}
	@Test
	public final void testSetFill() {
		ElementBasic e = (ElementBasic) ele.get(3).clone();
		e.setFill(true);
		assertEquals(true, e.getFill());
	}
	@Test
	public final void testGetUrsprung() {
		ElementBasic.setUrsprung(new Point(0, 0));
		assertEquals(new Point(0, 0), ElementBasic.getUrsprung());
	}
	@Test
	public final void testSetUrsprung() {
		ElementBasic.setUrsprung(new Point(100, 100));
		assertEquals(new Point(100, 100), ElementBasic.getUrsprung());
	}
	@Test
	public final void testGetUrsprungElement() {
		assertEquals(new Point(0, 0), ele.get(3).getUrsprungElement());
	}
	// **************
	// Speicher
	// **************
	@Test
	public final void testSpeicherKonstruktor() {
		assertEquals(s1, new Speicher());
	}
	@Test
	public final void testHashCodeSpeicher() {
		assertEquals(29822, s1.hashCode());
	}
	@Test
	public final void testEqualsSpeicher() {
		assertEquals(true, s1.equals(new Speicher()));
	}
	@Test
	public final void testaddElement() {
		ArrayList<ElementBasic> eltest = new ArrayList<>();
		eltest.add(ele.get(0));
		assertEquals(eltest, s2.getElements());
	}
	@Test
	public final void testUndo() {
		Speicher test = new Speicher();
		test.addElement(ele.get(0));
		test.undo();
		assertEquals(new ArrayList<>(), test.getElements());
	}
	@Test
	public final void testRedo() {
		Speicher test = new Speicher();
		test.addElement(ele.get(0));
		test.undo();
		test.redo();
		assertEquals(s2.getElements(), test.getElements());
	}
	@Test
	public final void testGetElements() {
		ArrayList<ElementBasic> eltest = new ArrayList<>();
		eltest.add(ele.get(0));		
		assertEquals(eltest, s2.getElements());
	}
	@Test
	public final void testGetBackground() {
		assertEquals(null, s2.getBackground());
	}
	@Test
	public final void testSetBackground() {
		s2.setBackground(Color.BLACK);
		assertEquals(Color.BLACK, s2.getBackground());
	}
	
}
