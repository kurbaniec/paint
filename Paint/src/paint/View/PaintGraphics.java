package paint.View;

import paint.model.ElementBasic;
import paint.model.Ellipse;
import paint.model.Rechteck;
import paint.model.RechteckAG;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Diese Panel dient zum Zeichnen der Elemente.
 * @author Kacper Urbaniec
 * @version 27.05.2018
 */
public class PaintGraphics extends JPanel {
    private ArrayList<ElementBasic> el;

    /**
     * Erzeugt ein neues PaintGraphics-Objekt indem es die
     * ArrayList initialisiert.
     */
    public PaintGraphics() {
        el = new ArrayList<>();
    }

    /**
     * Gibt die ArrayList der Elemente zurück.
     * @return Die ArrayList der Elemente
     */
    public ArrayList<ElementBasic> getFiguren() {
        return el;
    }

    /**
     * Gibt dem Grafikpanel ein weiteres Element zum zeichnen.
     * @param e Das zu zeichnende Element
     */
    public void addElement(ElementBasic e) {
        el.add(e);
    }

    /**
     * Löscht ein zu zeichnendes Element.
     * @param index Index des Elementes
     */
    public void delElement(int index) {
        el.remove(index);
    }

    /**
     * Löscht das zuletzt zu zeichnendes Element.
     */
    public void delElement() {
        if(el.size() > 0) {
            el.remove(el.size() - 1);
        }
    }

    /**
     * Ändert die Hintergrundfarbe des PaintGraphics-Panel.
     * @param c Die neue Hintergrundfarbe
     */
    public void setBackgroundColor(Color c) {
        this.setBackground(c);
    }

    /**
     * Verschiebt das letze Element in die Home Position
     */
    public void setHomePosition() {
        ElementBasic e = el.get(el.size()-1);
        e.setHomePosition();
    }

    /**
     * Setzt das Attribut el auf den Wert des übergebenen Parameters elements.
     * @param elements Die neue ArrayList mit allen Elementen
     */
    public void setEl(ArrayList<ElementBasic> elements) {
        this.el = elements;
        // Ursprung neu setzen für korrektes Zeichen
        if(el.size() > 0) {
            Point ur = el.get(el.size()-1).getUrsprungElement();
            ElementBasic.setUrsprung(ur);
        }
    }

    /**
     * Gibt das letzte Element der zu zeichnenden Objekte zurück.
     * @return Das letzte zu zeichnende Element
     */
    public ElementBasic getLastElement() {
        return el.get(el.size()-1);
    }

    /**
     * Verschiebt Element auf Zeichenfläche
     * positiver Int Wert nach rechts unten,  negativer int Wert nach links oben
     * @param xdir wie weit in x Richtung
     * @param ydir wie weit in y Richtung
     */
    public void verschieben(int xdir, int ydir) {
        ElementBasic e = el.get(el.size()-1);
        e.verschieben(xdir, ydir, this.getWidth(), this.getHeight());
    }

    /**
     * Diese Methode wird aufgerufen wenn GUI-Komponente angezeigt werden.
     * @param g - Das zu zeichnende Graphics-Objekt
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        int size = el.size();
        int last = size-1;
        for(int i = 0; i < size; i++) {
            if(i == last) {
                if(el.get(i) instanceof Rechteck || el.get(i) instanceof Ellipse || el.get(i) instanceof RechteckAG)
                el.get(i).rearrange();
            }
            el.get(i).draw(g);

        }
    }
}
