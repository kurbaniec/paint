package paint.View;

import javax.swing.*;

/**
 * Erstellt eine Frame für die GUI.
 * @author Kacper Urbaniec
 * @version 27.05.2018
 */
public class PaintFrame extends JFrame {
    /**
     * Initialisiert das JFrame.
     * @param titel - Titel des Fensters
     * @param panel - Das zu einbettete UhrPanel
     */
    public PaintFrame(String titel, JPanel panel) {
        this.setTitle(titel);
        this.setBounds(0, 0, 1280, 720);
        this.add(panel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
}
