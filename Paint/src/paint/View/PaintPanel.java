package paint.View;

import paint.Controller.PaintController;
import paint.model.ElementBasic;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.security.Key;
import java.util.ArrayList;

/**
 * Diese Klasse erzeugt ein JPanel, das die grafische Oberfläche für
 * das Paint-Programm bildet.
 * @author Kacper Urbaniec
 * @version 27.05.2018
 */
public class PaintPanel extends JPanel {
    private PaintGraphics el;
    private PaintController controller;
    private JRadioButtonMenuItem[] auswahl;
    private JMenuItem stift;
    private JMenuItem hinterg;
    private Color stiftC = Color.BLACK;
    private Color hintergC = Color.BLACK;
    private JColorChooser sChooser;
    private JColorChooser hChooser;

    /**
     * Gibt ein Element zum PaintGraphic hinzu.
     * @param e Das zu hinzufügende Element
     */
    public void addElement(ElementBasic e) {
        el.addElement(e);
        this.revalidate();
        this.repaint();
    }

    /**
     * Löscht ein zu zeichnendes Element.
     * @param index Index des Elementes
     */
    public void delElement(int index) {
        el.delElement(index);
        this.revalidate();
        this.repaint();
    }

    /**
     * Löscht das zuletzt zu zeichnendes Element.
     */
    public void delElement() {
        el.delElement();
        this.revalidate();
        this.repaint();
    }

    /**
     * Ändert die Hintergrundfarbe des PaintGraphics-Panel.
     * @param c Die neue Hintergrundfarbe
     */
    public void setBackgroundColor(Color c) {
        el.setBackgroundColor(c);
    }

    /**
     * Zeigt das Dialog zum Auswählen der Farbe für den Stift an.
     * @return - Der ausgewählte Color-Wert
     */
    public Color showSChooser() {
        JDialog dialog = JColorChooser.createDialog(null, "Stiftfarbe",true, sChooser, controller
                , controller);
        dialog.setVisible(true);
        stiftC = sChooser.getColor();
        return sChooser.getColor();
    }

    /**
     * Zeigt das Dialog zum Auswählen der Farbe für den Hintergrund an.
     * @return - Der ausgewählte Color-Wert
     */
    public Color showHChooser() {
        JDialog dialog = JColorChooser.createDialog(null,  "Hintergrundfarbe",true, hChooser, controller
                , controller);
        dialog.setVisible(true);
        hintergC = hChooser.getColor();
        return hChooser.getColor();
    }

    /**
     * Ändert die Hintergrundfarbe des Unterpunktes "Stift".
     * @param c Die neue Farbe
     */
    public void setStiftBackground(Color c) {
        stift.setBackground(c);
        this.repaint();
    }

    /**
     * Ändert die Hintergrundfarbe des Unterpunktes "Hintergrund".
     * @param c Die neue Farbe
     */
    public void setHintergBackground(Color c) {
        hinterg.setBackground(c);
        this.repaint();
    }

    /**
     * Gibt die Stiftfarbe zurück.
     * @return Colorobjekt der Stiftfarbe.
     */
    public Color getStiftC() {
        return stiftC;
    }

    /**
     * Gibt die Hintergrundfarbe zurück.
     * @return Colorobjekt der Hintergrundfarbe.
     */
    public Color getHintergC() {
        return hintergC;
    }

    /**
     * Gibt aus welcher RadioButton für Figuren ausgewählt wurde.
     * @return ActionCommand der ausgewählten Figur.
     */
    public String getAuswahl() {
        String s = "";
        for(JRadioButtonMenuItem b : auswahl) {
            if(b.isSelected()) {
                s = b.getActionCommand();
                break;
            }
        }
        return s;
    }

    /**
     * Gibt die Breite der Zeichenfläche zurück.
     * @return Die Breite als int-Wert
     */
    public int getGraphicsWidth() {
        return el.getWidth();
    }

    /**
     * Gibt die Höhe der Zeichenfläche zurück.
     * @return Die Höhe als int-Wert
     */
    public int getGraphicsHeight() {
        return el.getHeight();
    }

    /**
     * Verschiebt das letze Element in die Home Position
     */
    public void setHomePosition() {
        el.setHomePosition();
        this.revalidate();
        this.repaint();
    }

    /**
     * Verschiebt Element auf Zeichenfläche
     * positiver Int Wert nach rechts unten,  negativer int Wert nach links oben
     * @param xdir wie weit in x Richtung
     * @param ydir wie weit in y Richtung
     */
    public void verschieben(int xdir, int ydir) {
        el.verschieben(xdir, ydir);
        this.validate();
        this.repaint();
    }

    /**
     * Setzt die in PaintGraphics zu zeichnenden Elemente auf die Elemente des
     * Parameters elements.
     * @param elements Die neue ArrayList mit allen Elementen
     */
    public void setElements(ArrayList<ElementBasic> elements) {
        el.setEl(elements);
        this.validate();
        this.repaint();
    }

    /**
     * Gibt das letzte Element der zu zeichnenden Objekte zurück.
     * @return Das letzte zu zeichnende Element
     */
    public ElementBasic getLastElement() {
        return el.getLastElement();
    }


    /**
     * Erzeugt das Panel für das Frame.
     * @param controller Regelt die Ereignissteuerung
     */
    public PaintPanel(PaintController controller) {
        this.controller = controller;
        // Layout festlegen
        BorderLayout mainL = new BorderLayout();
        this.setLayout(mainL);
        // Menübar erstellen
        JMenuBar mb = new JMenuBar();
        // Menüpunkt Datei
        JMenu datei = new JMenu("Datei");
        mb.add(datei);
        // - Neu
        JMenuItem neu = new JMenuItem("Neu");
        neu.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_N, ActionEvent.CTRL_MASK));
        neu.addActionListener(controller);
        neu.setActionCommand("neu");
        datei.add(neu);
        // - Laden
        JMenuItem laden = new JMenuItem("Laden");
        laden.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_L, ActionEvent.CTRL_MASK));
        laden.addActionListener(controller);
        laden.setActionCommand("laden");
        datei.add(laden);
        // - Speichern
        JMenuItem speichern = new JMenuItem("Speichern");
        speichern.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_S, ActionEvent.CTRL_MASK));
        speichern.addActionListener(controller);
        speichern.setActionCommand("speichern");
        datei.add(speichern);
        // Menüpunkt Bearbeiten
        JMenu bearb = new JMenu("Bearbeiten");
        mb.add(bearb);
        // - Element löschen
        JMenuItem loe = new JMenuItem("Element löschen");
        loe.setMnemonic(KeyEvent.VK_Z);
        loe.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_Z, ActionEvent.CTRL_MASK));
        loe.addActionListener(controller);
        loe.setActionCommand("loe");
        bearb.add(loe);
        // - Element wiederherstellen
        JMenuItem wied = new JMenuItem("Element wiederherstellen");
        wied.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_Y, ActionEvent.CTRL_MASK));
        wied.addActionListener(controller);
        wied.setActionCommand("wied");
        bearb.add(wied);
        // - Element duplizieren
        JMenuItem dup = new JMenuItem("Element duplizieren");
        dup.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_O, ActionEvent.CTRL_MASK));
        dup.addActionListener(controller);
        dup.setActionCommand("dup");
        bearb.add(dup);
        // - Seperator
        bearb.addSeparator();
        // - Element in Home Position
        JMenuItem homepos = new JMenuItem("Element in Home Position");
        homepos.addActionListener(controller);
        homepos.setActionCommand("homepos");
        bearb.add(homepos);
        // - Elementfarbe ändern
        JMenuItem efarbe = new JMenuItem("Elementfarbe ändern");
        efarbe.addActionListener(controller);
        efarbe.setActionCommand("efarbe");
        bearb.add(efarbe);
        // Menüpunkt Zeichen
        JMenu zeichnen = new JMenu("Zeichnen");
        mb.add(zeichnen);
        ButtonGroup group = new ButtonGroup();
        auswahl = new JRadioButtonMenuItem[10];
        // - Freihand zeichnen
        auswahl[0] = new JRadioButtonMenuItem("Freihand");
        auswahl[0].addActionListener(controller);
        auswahl[0].setActionCommand("freih");
        group.add(auswahl[0]);
        zeichnen.add(auswahl[0]);
        // - Linien zeichnen
        auswahl[1] = new JRadioButtonMenuItem("Linie zeichnen");
        auswahl[1].addActionListener(controller);
        auswahl[1].setActionCommand("linie");
        group.add(auswahl[1]);
        zeichnen.add(auswahl[1]);
        // - Rechtecke zeichnen
        auswahl[2] = new JRadioButtonMenuItem("Rechtecke zeichnen");
        auswahl[2].addActionListener(controller);
        auswahl[2].setActionCommand("rechteck");
        group.add(auswahl[2]);
        zeichnen.add(auswahl[2]);
        // - Rechtecke abger. zeichen
        auswahl[3] = new JRadioButtonMenuItem("Rechtecke abger. zeichnen");
        auswahl[3].addActionListener(controller);
        auswahl[3].setActionCommand("rechteck2");
        group.add(auswahl[3]);
        zeichnen.add(auswahl[3]);
        // - Ellipsen zeichnen
        auswahl[4] = new JRadioButtonMenuItem("Ellipsen zeichnen");
        auswahl[4].addActionListener(controller);
        auswahl[4].setActionCommand("ellipse");
        group.add(auswahl[4]);
        zeichnen.add(auswahl[4]);
        // - Polygone zeichen
        auswahl[5] = new JRadioButtonMenuItem("Polygone zeichnen");
        auswahl[5].addActionListener(controller);
        auswahl[5].setActionCommand("polygone");
        group.add(auswahl[5]);
        zeichnen.add(auswahl[5]);
        // - Seperator
        zeichnen.addSeparator();
        // - Rechtecke ausmalen
        auswahl[6] = new JRadioButtonMenuItem("Rechtecke ausmalen");
        auswahl[6].addActionListener(controller);
        auswahl[6].setActionCommand("rechteckA1");
        group.add(auswahl[6]);
        zeichnen.add(auswahl[6]);
        // - Rechtecke abger. ausmalen
        auswahl[7] = new JRadioButtonMenuItem("Rechtecke abger. ausmalen");
        auswahl[7].addActionListener(controller);
        auswahl[7].setActionCommand("rechteckA2");
        group.add(auswahl[7]);
        zeichnen.add(auswahl[7]);
        // - Ellipsen ausmalen
        auswahl[8] = new JRadioButtonMenuItem("Ellipsen ausmalen");
        auswahl[8].addActionListener(controller);
        auswahl[8].setActionCommand("ellipseA");
        group.add(auswahl[8]);
        zeichnen.add(auswahl[8]);
        // - Polygone ausmalen
        auswahl[9] = new JRadioButtonMenuItem("Polygone ausmalen");
        auswahl[9].addActionListener(controller);
        auswahl[9].setActionCommand("polygoneA");
        group.add(auswahl[9]);
        zeichnen.add(auswahl[9]);
        // Menüpunkt Farbe
        JMenu farbe = new JMenu("Farbe");
        mb.add(farbe);
        // - Stift
        stift = new JMenuItem("Stift");
        sChooser = new JColorChooser();
        stift.addActionListener(controller);
        stift.setActionCommand("stift");
        farbe.add(stift);
        // - Hintergrund
        hinterg = new JMenuItem("Hintergrund");
        hChooser = new JColorChooser();
        hinterg.addActionListener(controller);
        hinterg.setActionCommand("hinterg");
        farbe.add(hinterg);
        // Führt dazu, dass das nächste Menü rechtsbündig ist
        mb.add(Box.createHorizontalGlue());
        // Info
        JMenu info = new JMenu("Info");
        mb.add(info);
        // - Hilfe
        JMenuItem hilfe = new JMenuItem("Hilfe");
        hilfe.addActionListener(controller);
        hilfe.setActionCommand("hilfe");
        info.add(hilfe);
        // - About
        JMenuItem about = new JMenuItem("About");
        about.addActionListener(controller);
        about.setActionCommand("about");
        info.add(about);
        // PaintGraphics-Panel hinzufügen
        el = new PaintGraphics();
        el.addMouseListener(controller);
        el.addMouseMotionListener(controller);
        el.addKeyListener(controller);
        el.setFocusable(true);
        this.addKeyListener(controller);
        this.setFocusable(true);
        // Alles hinzufügen
        this.add(el, BorderLayout.CENTER);
        this.add(mb, BorderLayout.PAGE_START);
    }

}
