package paint.model;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Die Klasse Speicher dient zum Zwischenspeichern von fertigen Elementen, zur
 * Regelung von "Element löschen" und "Element wiederherstellen" und zum Speichern
 * und Laden des Zeichenbrettes in eine Datei.
 * @author Kacper Urbaniec
 * @version 02.06.2018
 */
public class Speicher implements Serializable {
    private ArrayList<ElementBasic> el; // Speichert alle fertigen Elemente
    private int index = 0; // Ist der Index, ab dem nicht gewollte Elemente vorhanden sind
    private Color background; // Hintergrundfarbe des Zeichenbrettes

    /**
     * Erzeugt ein neues Speicher-Objekt.
     */
    public Speicher() {
        el = new ArrayList<>();
    }

    /**
     * GIbt ein fertiges Element dem Speicher zu.
     * @param e Das zu hinzufügende Element
     */
    public void addElement(ElementBasic e) {
        // Nach einem, oder mehreren "Element löschen" Befehlen, sollen nicht
        // gewollten Elemente gelöscht werden, falls ein neues Element
        // hinzugefüt wird
        if(index < el.size()) {
            el.subList(index, el.size()).clear();
        }
        // Element hinzufügen und Index erhöhen
        el.add(index, e);
        index++;
    }

    /**
     * Führt den "Element löschen"-Befehl aus. Nicht gewollte Elemente befinden sich
     * dann, nach dem Index.
     */
    public void undo() {
        if(index != 0) {
            index--;
        }
    }

    /**
     * Führt dem "Element wiederherstellen"-Befehl aus. Ein nicht gewolltes Element,
     * das sich nach dem Index befindet, kommt wieder vor den Index.
     *
     */
    public void redo() {
        if(index < el.size()) {
            index++;
        }
    }

    /**
     * Gibt alle gewollten Elemente, also alle Element vor dem Index, als ArrayList
     * zurück.
     * @return ArrayList mit allen gewollten Elementen
     */
    public ArrayList<ElementBasic> getElements() {
        return new ArrayList<>(el.subList(0, index));
    }

    /**
     * Gibt die Hintergrundfarbe des Zeichenbrettes zurück.
     * @return Hintergrundfarbe des Zeichenbrettes
     */
    public Color getBackground() {
        return background;
    }

    /**
     * Setzt die Hintergrundfarbe auf den Wert des Parameters
     * @param background Die neue Hintergrundfarbe
     */
    public void setBackground(Color background) {
        this.background = background;
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((background == null) ? 0 : background.hashCode());
		result = prime * result + ((el == null) ? 0 : el.hashCode());
		result = prime * result + index;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Speicher other = (Speicher) obj;
		if (background == null) {
			if (other.background != null)
				return false;
		} else if (!background.equals(other.background))
			return false;
		if (el == null) {
			if (other.el != null)
				return false;
		} else if(el.size() != other.el.size()) {
			return false;
		} else {
			for(int i = 0; i < el.size(); i++) {
				if(!el.get(i).equals(other.el.get(i)))
					return false;
			}
		}
		if (index != other.index)
			return false;
		return true;
	}
}
