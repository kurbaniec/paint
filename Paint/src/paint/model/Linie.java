package paint.model;

import java.awt.*;
import java.util.*;

import paint.Element;

/**
 * Speichert die Daten f�r eine Linie
 * Die Linie geht vom ersten Punkt der ArrayList bis zum zweiten Punkt der ArrayList
 * @author Martin Wustinger
 * @version 2018-05-18
 */
public class Linie extends ElementBasic {
	
	public Linie() {
		color = Color.BLUE;
		points = new ArrayList<>();
		points.add(new Point(0, 0));
		points.add(new Point(100, 100));
	}
	public Linie(Color color) {
		this.color = color;
		points = new ArrayList<>();
		points.add(new Point(0, 0));
		points.add(new Point(100, 100));
	}
	public Linie(Color color, ArrayList<Point> points) {
		this.color = color;
		this.points = points;
	}
	
	/**
	 * Zeichnet ein Linie Element
	 */
	@Override
	public void draw(Graphics g) {
		g.setColor(color);
		g.drawLine(points.get(0).x, points.get(0).y, points.get(1).x, points.get(1).y);
	}
	/**
	 * Klont Das Element
	 */
	@Override
	public Element clone() {
		Color c = new Color(color.getRGB());
		ArrayList<Point> pts = this.pointClone();
		return new Linie(c, pts);
	}
}























