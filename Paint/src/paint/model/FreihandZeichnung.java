package paint.model;

import java.awt.*;
import java.util.*;

import paint.Element;

/**
 * Speichert die Daten f�r eine FreihandZeichnung
 * @author Martin Wustinger
 * @version 2018-05-18
 */
public class FreihandZeichnung extends ElementBasic {
	
	public FreihandZeichnung() {
		color = Color.BLUE;
		points = new ArrayList<>();
	}
	public FreihandZeichnung(Color color) {
		this.color = color;
		points = new ArrayList<>();
	}
	public FreihandZeichnung(Color color, ArrayList<Point> points) {
		this.color = color;
		this.points = points;
	}
	
	/**
	 * Zeichnet ein FreihandZeichnung Element
	 */
	@Override
	public void draw(Graphics g) {
		g.setColor(color);
		int x1, y1, x2, y2;
		Point p;
		for(int i = 0; i < points.size()-1; i++) {
			p = points.get(i);
			x1 = p.x;
			y1 = p.y;
			p = points.get(i+1);
			x2 = p.x;
			y2 = p.y;
			g.drawLine(x1, y1, x2, y2);
		}
	}
	/**
	 * Klont Das Element
	 */
	@Override
	public Element clone() {
		Color c = new Color(color.getRGB());
		ArrayList<Point> pts = this.pointClone();
		return new FreihandZeichnung(c, pts);
	}
}























