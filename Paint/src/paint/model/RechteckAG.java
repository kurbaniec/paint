package paint.model;

import java.awt.*;
import java.util.*;

import paint.Element;

/**
 * Speichert die Daten f�r eine RechteckAG
 * Der erste gespeicherte Punkt ist der Punkt links oben des RechteckAGs
 * Der zweite gespeicherte Punkt ist der Punkt rechts unten des RechteckAGs
 * @author Martin Wustinger
 * @version 2018-05-18
 */
public class RechteckAG extends ElementBasic {
	
	public RechteckAG() {
		color = Color.BLUE;
		points = new ArrayList<>();
		fill = false;
	}
	public RechteckAG(Color color) {
		this.color = color;
		points = new ArrayList<>();
		fill = false;
	}
	public RechteckAG(Color color, ArrayList<Point> points) {
		this.color = color;
		this.points = points;
		fill = false;
	}
	public RechteckAG(Color color, ArrayList<Point> points, boolean fill) {
		this.color = color;
		this.points = points;
		this.fill = fill;
	}
	
	/**
	 * Zeichnet ein RechteckAG Element
	 */
	@Override
	public void draw(Graphics g) {
		g.setColor(color);
		int b = points.get(1).x-points.get(0).x;
		int h = points.get(1).y-points.get(0).y;
		if(fill) {
			g.fillRoundRect(points.get(0).x, points.get(0).y, b, h, b<h?b:h, b<h?b:h);
		}else {
			g.drawRoundRect(points.get(0).x, points.get(0).y, b, h, b<h?b:h, b<h?b:h);
		}
	}
	/**
	 * Klont Das Element
	 */
	@Override
	public Element clone() {
		Color c = new Color(color.getRGB());
		ArrayList<Point> pts = this.pointClone();
		return new RechteckAG(c, pts, fill);
	}
}
