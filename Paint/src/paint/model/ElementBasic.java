package paint.model;

import java.awt.*;
import java.io.Serializable;
import java.util.*;
import paint.Element;

/**
 * Liefert alle Methoden und Attribute die alle Elemente brauchen
 * @author Martin Wustinger
 * @version 2018-05-18
 */
public abstract class ElementBasic implements Element, Serializable {
	protected Color color;				//Speichert die Farbe in der das Element gezeichnet werden soll
	protected ArrayList<Point> points;	//Speichert die Punkte die das Element beschreibt
	protected boolean fill = false;		//Wenn diese Flag auf 'true' gesetzt ist wird die 'fill'-Methode aufgerufen, wenn sie auf 'false' ist wird die 'draw'-Methode aufgerufen 
	protected static Point ursprung = new Point(0, 0);	//Der Punkt welcher Als aller Erster gespeichert wird von dem das Element dann aus geht
	/**
	 * F�gt Punkt zu ArrayList hinzu
	 */
	@Override
	public void addPoint(int x, int y) {
		Point p = new Point(x, y);
		if(checkObAufZF(p)) {
			points.add(p);
		}
	}
	/**
	 * L�scht den letzten Punkt
	 */
	@Override
	public void removeLastPoint() {
		points.remove(points.size()-1);
	}
	/**
	 * �ndert die Koordinaten eines Punktes aus der ArrayList
	 * @param index welcher Punkt ge�ndert werden soll
	 * @param newX neuer X-Wert
	 * @param newY neuer Y-Wert
	 */
	public void changePoint(int index, int newX, int newY) {
		points.set(index, new Point(newX, newY));
	}
	
	//Braucht man damit Java keinen Fehler auswirft
	@Override
	public abstract Element clone();
	
	/**
	 * �berpr�ft ob Punkt auf Zeichenfl�che liegt
	 * @param p der Punkt der �berpr�ft wird
	 */
	public static boolean checkObAufZF(Point p) {
		//Ausmessungen der Zeichenfl�che
		int[] ausmessungen = {0, 10000, 0, 10000}; //Hier kommt dann eine Methode hin die Sich aus der View die Ausmessungen der Zeichenfl�che holt R�ckgabe int-Array Aufbau {minX, maxX, minY, maxY}
		if(p.x < ausmessungen[0] || p.x > ausmessungen[1] || p.y < ausmessungen[2] || p.y > ausmessungen[3]) {
			return false;
		}
		return true;
	}
	
	/**
	 * Verschiebt Element auf Zeichenfl�che
	 * positiver Int Wert nach rechts unten,  negativer int Wert nach links oben
	 * @param xdir wie weit in x Richtung
	 * @param ydir wie weit in y Richtung
	 * @param width Die Breite des Zeichen-JPanels
	 * @param height Die H�he des Zeichen-JPanels
	 */
	public void verschieben(int xdir, int ydir, int width, int height) {
		//�berpr�fen ob Element �berhaupt so weit verschoben werden kann
		//�u�ere Abmessung des Elements herrausfinden
		int minX = points.get(0).x;
		int maxX = points.get(0).x;
		int minY = points.get(0).y;
		int maxY = points.get(0).y;
		for(Point p : points) {
			if(p.x < minX) {
				minX = p.x;
			}else if(p.x > maxX) {
				maxX = p.x;
			}
			if(p.y < minY) {
				minY = p.y;
			}else if(p.y > maxY) {
				maxY = p.y;
			}
		}
		int[] ausmessungen = {0, width, 0, height}; //Hier kommt dann eine Methode hin die sich aus der View die Ausmessungen der Zeichenfl�che holt R�ckgabe int-Array Aufbau {minX, maxX, minY, maxY}
		if(minX+xdir<ausmessungen[0]) {
			xdir=minX-ausmessungen[0];
		}else if(maxX+xdir>ausmessungen[1]) {
			xdir=ausmessungen[1]-maxX;
		}
		if(minY+ydir<ausmessungen[2]) {
			ydir=minY-ausmessungen[2];
		}else if(maxY+ydir>ausmessungen[3]) {
			ydir=ausmessungen[3]-maxY;
		}
		for(Point p : points) {
			p.x+=xdir;
			p.y+=ydir;
		}
		ursprung.setLocation(points.get(0).x, points.get(0).y);

	}
	
	/**
	 * Verschiebt Element so weit wie m�glich nach links oben
	 */
	@Override
	public void setHomePosition() {
		//�u�ere Abmessung des Elements herrausfinden
		int minX = points.get(0).x;
		int minY = points.get(0).y;
		for(Point p : points) {
			if(p.x < minX) {
				minX = p.x;
			}
			if(p.y < minY) {
				minY = p.y;
			}
		}
		for(Point p : points) {
			p.x-=minX;
			p.y-=minY;
		}
		ursprung = new Point(0, 0);
	}


	/**
	 * Die Methode rearrange wird ben�tigt um das Zeichnen der Ellipsen und
	 * Rechtecke nach oben oder nach links zu realisieren.
	 */
	public void rearrange() {
		
		Point p0 = points.get(0);
		Point p1 = points.get(1);
		
		if(ursprung == null) {
			ursprung=(Point)p0.clone();
		}
		//P1 ist rechts unter ursprung
		if(ursprung.x < p1.x && ursprung.y < p1.y) {
			changePoint(0, ursprung.x, ursprung.y);
			changePoint(1, p1.x, p1.y);
			
		//P1 ist rechts �ber ursprung
		}else if(ursprung.x < p1.x && ursprung.y > p1.y) {
			changePoint(0, ursprung.x, p1.y);
			changePoint(1, p1.x, ursprung.y);
			
		//P1 ist links unter ursprung
		}else if(ursprung.x > p1.x && ursprung.y < p1.y) {
			changePoint(0, p1.x, ursprung.y);
			changePoint(1, ursprung.x, p1.y);
			
		//P1 ist links �ber ursprung		
		}else if(ursprung.x > p1.x && ursprung.y > p1.y) {
			changePoint(0, p1.x, p1.y);
			changePoint(1, ursprung.x, ursprung.y);
		}
	}

	/**
	 * Erzeugt eine tiefe Kopie der Punkte-ArrayList.
	 * @return Tiefe Kopie der Punkte ArrayList
	 */
	public ArrayList<Point> pointClone() {
		ArrayList<Point> pts = new ArrayList<>();
		for(Point p : points) {
			Point pt = new Point((int)p.getX(), (int)p.getY());
			pts.add(pt);
		}
		return pts;
	}
	
	/** 
	 * HashCode Methode
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + (fill ? 1231 : 1237);
		result = prime * result + ((points == null) ? 0 : points.hashCode());
		return result;
	}
	/**
	 * Equals Methode
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ElementBasic other = (ElementBasic) obj;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		if (fill != other.fill)
			return false;
		if (points == null) {
			if (other.points != null)
				return false;
		} else if(points.size() != other.points.size()) {
			return false;
		} else {
			for(int i = 0; i < points.size(); i++) {
				if(!points.get(i).equals(other.points.get(i)))
					return false;
			}
		}
		return true;
	}
	/**
	 * Getter und Setter f�r Color
	 */
	@Override
	public Color getColor() {
		return color;
	}
	@Override
	public void setColor(Color c) {
		color = c;
	}

	/**
	 * Getter und Setter f�r fill
	 */
	public boolean getFill() {
		return fill;
	}
	public void setFill(boolean fill) {
		this.fill = fill;
	}
	
	/**
	 * Getter und Setter f�r ursprung
	 */
	public static Point getUrsprung() {
		return ursprung;
	}
	public static void setUrsprung(Point p) {
		ursprung = p;
	}

	/**
	 * Getter um den Ursprung eines Elementes zu erhalten
	 */
	public Point getUrsprungElement() {
		return this.points.get(0);
	}
}
