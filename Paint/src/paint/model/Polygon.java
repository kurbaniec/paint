package paint.model;

import java.awt.*;
import java.util.*;

import paint.Element;

/**
 * Speichert die Daten f�r eine Polygon
 * In der ArrayList werden die ganzen Punkte des Polygons gespeichert 
 * in der Reihenfolge in der sie dann auch gezeichnet werden sollen
 * @author Martin Wustinger
 * @version 2018-05-18
 */
public class Polygon extends ElementBasic {
	
	public Polygon() {
		color = Color.BLUE;
		points = new ArrayList<>();
		fill = false;
	}
	public Polygon(Color color) {
		this.color = color;
		points = new ArrayList<>();
		fill = false;
	}
	public Polygon(Color color, ArrayList<Point> points) {
		this.color = color;
		this.points = points;
		fill = false;
	}
	public Polygon(Color color, ArrayList<Point> points, boolean fill) {
		this.color = color;
		this.points = points;
		this.fill = fill;
	}
	
	/**
	 * Zeichnet ein Polygon Element
	 */
	@Override
	public void draw(Graphics g) {
		g.setColor(color);
		int[] xPoints = new int[points.size()];
		int[] yPoints = new int[points.size()];
		for(int i = 0; i < points.size(); i++) {
			xPoints[i] = points.get(i).x;
			yPoints[i] = points.get(i).y;
		}
		if(fill) {
			g.fillPolygon(xPoints, yPoints, points.size());
		}else {
			g.drawPolygon(xPoints, yPoints, points.size());
		}
	}
	/**
	 * Klont Das Element
	 */
	@Override
	public Element clone() {
		Color c = new Color(color.getRGB());
		ArrayList<Point> pts = this.pointClone();
		return new Polygon(c, pts, fill);
	}
}























