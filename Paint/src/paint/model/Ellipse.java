package paint.model;

import java.awt.*;
import java.util.*;

import paint.Element;

/**
 * Speichert die Daten f�r eine Ellipse
 * Eine Ellipse setzt sich aus einer ArrayList zusammen
 * Der erste gespeicherte Punkt ist der Punkt links oben des Rechtecks welches die Ellipse umschlie�t
 * Der zweite gespeicherte Punkt ist der Punkt rechts unten des Rechtecks welches die Ellipse umschlie�t
 * @author Martin Wustinger
 * @version 2018-05-18
 */
public class Ellipse extends ElementBasic {
	
	public Ellipse() {
		color = Color.BLUE;
		points = new ArrayList<>();
		fill = false;
	}
	public Ellipse(Color color) {
		this.color = color;
		points = new ArrayList<>();
		fill = false;
	}
	public Ellipse(Color color, ArrayList<Point> points) {
		this.color = color;
		this.points = points;
		fill = false;
	}
	public Ellipse(Color color, ArrayList<Point> points, boolean fill) {
		this.color = color;
		this.points = points;
		this.fill = fill;
	}
	
	/**
	 * Zeichnet ein Ellipse Element
	 */
	@Override
	public void draw(Graphics g) {
		g.setColor(color);
		if(fill) {
			g.fillOval(points.get(0).x, points.get(0).y, points.get(1).x-points.get(0).x, points.get(1).y-points.get(0).y);
		}else {
			g.drawOval(points.get(0).x, points.get(0).y, points.get(1).x-points.get(0).x, points.get(1).y-points.get(0).y);
		}
	}
	/**
	 * Klont Das Element
	 */
	@Override

	public Element clone() {
		Color c = new Color(color.getRGB());
		ArrayList<Point> pts = this.pointClone();
		return new Ellipse(c, pts, fill);
	}
}























