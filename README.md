# paint
Paint ist ein einfaches Programm, welches dem beliebten Zeichenprogramm Paint nachempfunden ist.

## Hintergrund
Das Programm war eine Aufgabenstellung für zwei Personen im dritten Jahrgang, Ich habe es zusammen mit meinem Mitschüler Martin Wustigner erstellt. Geschrieben ist das Programm in der Programmiersprache Java und setzt das MVC-Prinzip praktisch um.

## Start
Zum Starten der Anwendung muss nur die PaintController-Klasse gestartet werden.

## Funktionen
Alle Funktionen in Form einer Anleitung sind unter dem  Punkt *Hilfe* in der grafischen Oberfläche zu finden. 

# Orginale Aufgabenstellung
Wir wollen ein Zeichenbrett entwicklen, welches eine einfache Paint-Applikation darstellt.
Die Anforderungen werden im Moodle-System auch grafisch genauer erklaert.

Als Basis dient das Interface Element mit allen notwenigen Funktionen eines Zeichenobjektes

Grundanforderungen:

    Laden und Speichern eines Files
    Alle Zeichenobjekte zeichnen
    Stift- und Hintergrundfarbe anzeigen und setzen
    Letztes Element dupplizieren
    Letztes Element in die Homeposition verschieben (Soweit nach links oben wie moeglich)
    Info
    About


Erweiterte Anforderungen:

    Rueckgaengmachen und Wiederherstellen aller Aktionen (auch nach dem Laden)
    Tastenkuerzel (Strg-z, Strg-y,...)
    Verschieben des letzten Objektes (Hinauf, Hinunter, Rechts, Links,...)
